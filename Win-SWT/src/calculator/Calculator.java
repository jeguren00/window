package calculator;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;

public class Calculator {
	private static Text disp;
	private static Button btnCe;
	private static Button lessThan;
	private static Button module;
	private static Button powf;
	private static Button num7;
	private static Button num8;
	private static Button num9;
	private static Button div;
	private static Button num4;
	private static Button num5;
	private static Button num6;
	private static Button mult;
	private static Button num1;
	private static Button num2;
	private static Button num3;
	private static Button less;
	private static Button num0;
	private static Button dot;
	private static Button equal;
	private static Button sum;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Shell shell = new Shell();
		shell.setSize(311, 399);
		shell.setText("SWT Application");
		
		num7 = new Button(shell, SWT.NONE);
		num7.setBounds(51, 133, 34, 34);
		num7.setText("7");
		num7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "7") ;
			}
		});
		
		
		num8 = new Button(shell, SWT.NONE);
		num8.setText("8");
		num8.setBounds(101, 133, 34, 34);
		num8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "8") ;
			}
		});
		
		num9 = new Button(shell, SWT.NONE);
		num9.setText("9");
		num9.setBounds(150, 133, 34, 34);
		num9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "9") ;
			}
		});
		
		num4 = new Button(shell, SWT.NONE);
		num4.setText("4");
		num4.setBounds(51, 173, 34, 34);
		num4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "4") ;
			}
		});
		
		num5 = new Button(shell, SWT.NONE);
		num5.setText("5");
		num5.setBounds(101, 173, 34, 34);
		num5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "5") ;
			}
		});
		
		num6 = new Button(shell, SWT.NONE);
		num6.setText("6");
		num6.setBounds(150, 173, 34, 34);
		num6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "6") ;
			}
		});
		
		num1 = new Button(shell, SWT.NONE);
		num1.setText("1");
		num1.setBounds(51, 213, 34, 34);
		num1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "1") ;
			}
		});
		
		num2 = new Button(shell, SWT.NONE);
		num2.setText("2");
		num2.setBounds(101, 213, 34, 34);
		num2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "2") ;
			}
		});
		
		num3 = new Button(shell, SWT.NONE);
		num3.setText("3");
		num3.setBounds(150, 213, 34, 34);
		num3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "3") ;
			}
		});
		
		num0 = new Button(shell, SWT.NONE);
		num0.setText("0");
		num0.setBounds(51, 260, 34, 34);
		num0.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "0") ;
			}
		});
		
		dot = new Button(shell, SWT.NONE);
		dot.setText(".");
		dot.setBounds(101, 260, 34, 34);
		dot.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + ".") ;
			}
		});
		
		equal = new Button(shell, SWT.NONE);
		equal.setText("=");
		equal.setBounds(150, 260, 34, 34);
		equal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String display = disp.getText();
				ArrayList<Character> symb = new ArrayList<Character>();
				ArrayList<Double> nums = new ArrayList<Double>();
				
				int lastSymb = 0;

				for(int i = 0; i < display.length(); i++) {
					if (display.charAt(i) == '+' || display.charAt(i) == '-' || display.charAt(i) == '*' || display.charAt(i) == '/'|| display.charAt(i) == '^' || display.charAt(i) == '%') {
						Double number = Double.parseDouble(display.substring(lastSymb,i));
						symb.add(display.charAt(i));
						nums.add(number);
						lastSymb = i+1;
					}
					if (i == display.length()-1) {
						Double number = Double.parseDouble(display.substring(lastSymb));
						nums.add(number);
					}
				}

				int indexNums = 0;
				int indexNumsTwo = 1;
				Double result = 0.0;
				for (char c: symb) {
					if (c == '+') {
						result = result + (nums.get(indexNums) +  nums.get(indexNumsTwo));
						indexNums++;
						indexNumsTwo++;
					} else if (c == '-') {
						result = result + (nums.get(indexNums) -  nums.get(indexNumsTwo));
						indexNums++;
						indexNumsTwo++;
					} else if (c == '*') {
						result = result + (nums.get(indexNums) *  nums.get(indexNumsTwo));
						indexNums++;
						indexNumsTwo++;
					} else if (c == '/') {
						result = result + (nums.get(indexNums) /  nums.get(indexNumsTwo));
						indexNumsTwo++;
						indexNums++;
					} else if (c == '%') {
						result = result + (nums.get(indexNums) %  nums.get(indexNumsTwo));
						indexNumsTwo++;
						indexNums++;
					} else if (c == '^') {
						result = (Double) (result + (Math.pow(nums.get(indexNums), nums.get(indexNumsTwo))));
						indexNumsTwo++;
						indexNums++;
					}
				}
				disp.setText(String.valueOf(result));
			}
		});

		btnCe = new Button(shell, SWT.NONE);
		btnCe.setText("CE");
		btnCe.setBounds(51, 83, 34, 34);
		btnCe.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText("") ;
			}
		});
		
		lessThan = new Button(shell, SWT.NONE);
		lessThan.setText("<");
		lessThan.setBounds(101, 83, 34, 34);
		lessThan.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "<") ;
			}
		});
		
		module = new Button(shell, SWT.NONE);
		module.setText("%");
		module.setBounds(150, 83, 34, 34);
		module.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "%") ;
			}
		});
		
		disp = 	new Text(shell, SWT.BORDER);
		disp.setBounds(52, 34, 179, 30);
		
		div = new Button(shell, SWT.NONE);
		div.setText("/");
		div.setBounds(197, 133, 34, 34);
		div.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "/") ;
			}
		});
		
		mult = new Button(shell, SWT.NONE);
		mult.setText("*");
		mult.setBounds(197, 173, 34, 34);
		mult.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "*") ;
			}
		});
		
		less = new Button(shell, SWT.NONE);
		less.setText("-");
		less.setBounds(197, 213, 34, 34);
		less.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "-") ;
			}
		});		
		
		sum = new Button(shell, SWT.NONE);
		sum.setText("+");
		sum.setBounds(197, 260, 34, 34);
		sum.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				disp.setText(disp.getText() + "+") ;
			}
		});
		
		powf = new Button(shell, SWT.NONE);
		powf.setText("^");
		powf.setBounds(197, 83, 34, 34);

		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
	}
}
