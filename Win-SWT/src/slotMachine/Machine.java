/*
* Machine.java        01/04/2022
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation v3.
*
* @see <a href="http://www.gnu.org/licenses/gpl.html">GNU License</a> for more information.
*
* Copyright (c) Joan S�culi <a href="mailto:jseculi@escoladeltreball.org">Joan Seculi</a>
*/
package slotMachine;
/**
* Enter a description
*
* @author  <a href="mailto:jseculi@escoladeltreball.org">Joan Seculi</a>
* @version 1.0
* @since   01/04/2022  
* 
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation v3.
*
* @see <a href="http://www.gnu.org/licenses/gpl.html">GNU License</a>
*
*
*/
public class Machine {
	public static final String STRAWBERRY = "STRAWBERRY";
	public static final String BANANA = "BANANA";
	public static final String APPLE = "APPLE";
	public static final String LEMON = "LEMON";
	public static final String WATERMELON = "WATERMELON";
	public static final String STAR = "STAR";

	public static final String ONE_STAR = "ONE_STAR";
	public static final String TWO_FRUITS = "TWO_FRUITS";
	public static final String TWO_STARS = "TWO_STARS";
	public static final String THREE_FRUITS = "THREE_FRUITS";
	public static final String TWO_FRUITS_ONE_START = "TWO FRUITS ONE STAR";
	public static final String THREE_STARS = "THREE_STARS";
	public static final String LOST = "YOU HAVE LOST!!";
	public static final String BYE = "BYE!!";
	public static final String NO_MORE_MONEY = "SORRY! YOU DON\'T HAVE ENOUGH MONEY TO BET!!";

	public static final byte BET = 25;
	public static final float MONEY_MACHINE = 100000.0f;

	

	/**
	 * Returns the combination of the 3 fruits as text: 
	 * TWO_FRUITS, TWO_FRUITS_ONE_START, THREE_FRUITS, TWO_STARS, THREE_STARS, LOST
	 * 
	 * @param fruit1   first fruit
	 * @param fruit2   second fruit
	 * @param fruit3   third fruit
	 * @return         the prize in String
	 */
	public static String result(byte fruit1, byte fruit2, byte fruit3) {

		if (isTwoFruits(fruit1, fruit2, fruit3)) {
			return TWO_FRUITS;
		} else if (isTwoFruitsOneStar(fruit1, fruit2, fruit3)) {
			return TWO_FRUITS_ONE_START;
		} else if (isThreeFruits(fruit1, fruit2, fruit3)) {
			return THREE_FRUITS;
		} else if (isTwoStars(fruit1, fruit2, fruit3)) {
			return TWO_STARS;
		} else if (isThreeStars(fruit1, fruit2, fruit3)) {
			return THREE_STARS;
		} else {
			return LOST;
		}

	}

	

	/**
	 * Generates a random number between 1 and 6, the probability of 1 to 5 is double than number 6
	 * @return a random number between 1 and 6
	 */
	public static byte randomFruit() {

		/*
		 * 1: strawberry 20% 2: banana 20% 3: apple 20% 4: lemon 20% 5: watermelon 20%
		 * 6: star 10%
		 */
		// return (byte) (Math.random() * 6 + 1); //1 ..6
		byte prob = (byte) (Math.random() * 110 + 1); // 1..110
		if (prob > 0 && prob <= 20)
			return 1;
		else if (prob > 20 && prob <= 40)
			return 2;
		else if (prob > 40 && prob <= 60)
			return 3;
		else if (prob > 60 && prob <= 80)
			return 4;
		else if (prob > 80 && prob <= 100)
			return 5;
		else
			return 6;

	}

	public static boolean isOneStar(int fruit1, int fruit2, int fruit3) {
		// * x y
		// x * y
		// x y *
		return (fruit1 == 6 && (fruit1 != fruit2 && fruit1 != fruit3))
				|| (fruit2 == 6 && (fruit2 != fruit1 && fruit2 != fruit3))
				|| (fruit3 == 6 && (fruit3 != fruit1 && fruit3 != fruit2));
	}

	public static boolean isTwoFruits(int fruit1, int fruit2, int fruit3) {
		// x x y
		// y x x
		// x y x
		return (fruit1 == fruit2 && fruit1 != fruit3 && fruit1 != 6 && fruit3 != 6)
				|| (fruit2 == fruit3 && fruit2 != fruit1 && fruit2 != 6 && fruit1 != 6)
				|| (fruit3 == fruit1 && fruit3 != fruit2 && fruit3 != 6 && fruit2 != 6);
	}

	public static boolean isThreeFruits(int fruit1, int fruit2, int fruit3) {
		// x x x
		// x x x
		// x x x
		return (fruit1 == fruit2 && fruit2 == fruit3) && (fruit1 != 6); 
	}

	public static boolean isTwoStars(int fruit1, int fruit2, int fruit3) {
		// * * x
		// * x *
		// x * *
		return (fruit1 == fruit2 && fruit1 != fruit3 && fruit1 == 6)
				|| (fruit1 == fruit3 && fruit1 != fruit2 && fruit1 == 6)
				|| (fruit2 == fruit3 && fruit2 != fruit1 && fruit2 == 6);
	}

	public static boolean isThreeStars(int fruit1, int fruit2, int fruit3) {
		return (fruit1 == fruit2 && fruit2 == fruit3) && (fruit1 == 6);
	}

	public static boolean isTwoFruitsOneStar(int fruit1, int fruit2, int fruit3) {
		// x x *
		// * x x
		// x * x
		return (fruit1 == fruit2 && fruit3 == 6 && fruit1 != 6) || (fruit2 == fruit3 && fruit1 == 6 && fruit2 != 6)
				|| (fruit3 == fruit1 && fruit2 == 6 && fruit3 != 6);
	}

	
	

	
}
